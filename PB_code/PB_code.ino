#include <SPI.h>
#include <mcp2515.h>

struct can_frame MasterNoSleep;
struct can_frame MasterSleepPending;
struct can_frame MasterGoSleep;

struct can_frame canMsg;

#define ledPin 5

MCP2515 mcp2515(10);

bool decharging = true;
bool led = false;
void setup() {
    
  Serial.begin(9600);
  
  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, HIGH);
 
  
  MasterNoSleep.can_id  = 0x1ef01802 | CAN_EFF_FLAG;
  MasterNoSleep.can_dlc = 5;
  MasterNoSleep.data[0] = 0x01;
  MasterNoSleep.data[1] = 0x00;
  MasterNoSleep.data[2] = 0x00;
  MasterNoSleep.data[3] = 0x00;
  MasterNoSleep.data[4] = 0x00;

   MasterSleepPending.can_id  = 0x1ef01802 | CAN_EFF_FLAG;
  MasterSleepPending.can_dlc = 5;
  MasterSleepPending.data[0] = 0x02;
  MasterSleepPending.data[1] = 0x00;
  MasterSleepPending.data[2] = 0x00;
  MasterSleepPending.data[3] = 0x00;
  MasterSleepPending.data[4] = 0x00;

  MasterGoSleep.can_id  = 0x1ef01802 | CAN_EFF_FLAG;
  MasterGoSleep.can_dlc = 5;
  MasterGoSleep.data[0] = 0x03;
  MasterGoSleep.data[1] = 0x00;
  MasterGoSleep.data[2] = 0x00;
  MasterGoSleep.data[3] = 0x00;
  MasterGoSleep.data[4] = 0x00;

  
  mcp2515.reset();
  mcp2515.setBitrate(CAN_250KBPS, MCP_8MHZ);
  mcp2515.setNormalMode();

  cli();                   //Disable interrupts while setting registers       
  TCCR1A = 0;              // Make sure it is zero
  TCCR1B = (1 << WGM12);   // Configure for CTC mode (Set it; don't OR stuff into it)
  TCCR1B |= (1 << CS12); // Prescaler @ 1024
  TIMSK1 = (1 << OCIE1A);  // Enable interrupt
  OCR1A = 6250;           // compare value = 1 sec (16MHz AVR)
  sei();                   // Enable global interrupts

}


ISR(TIMER1_COMPA_vect){    //This is the interrupt request
  if(decharging){
    mcp2515.sendMessage(&MasterNoSleep);
  }
}

void loop() {

}

void ledSwitch(){
  Serial.println("Interrupt");
  if(led){
    digitalWrite(LED_BUILTIN, HIGH);
    led = false;
  } else {
      digitalWrite(LED_BUILTIN, LOW);
      led = true;
  }
}

void bmsShutdown(){
  Serial.println("Shutdown");
  decharging = false;
  for(int i = 0; i <= 3; i++){
      mcp2515.sendMessage(&MasterSleepPending);
      delay(100);
  }
   mcp2515.sendMessage(&MasterGoSleep);
}
